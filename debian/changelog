libterm-size-perl (0.211-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.
  * Import upstream version 0.211.
  * Add debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Mon, 18 Jan 2021 23:10:36 +0100

libterm-size-perl (0.209-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Damyan Ivanov ]
  * Mark remove_scary_notice.patch as forwarded to
    https://rt.cpan.org/Public/Bug/Display.html?id=41336

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Drop remove_scary_notice.patch, the POD has been updated.
  * Update years of upstream and packaging copyright.
  * Drop build dependency on libtest-pod-perl.
    The respective test is an author test now.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.2.1.
  * Bump debhelper compatibility level to 10.
  * Set bindnow linker flag in debian/rules.

 -- gregor herrmann <gregoa@debian.org>  Sun, 02 Sep 2018 23:43:47 +0200

libterm-size-perl (0.207-1) unstable; urgency=low

  [ Damyan Ivanov ]
  * New upstream release
  * watch: mangle upstream versions to always force three digits after
    the dot

  [ Martín Ferrari ]
  * debian/watch: changed the quoting to fit PET's understanding of syntax,
    screw uscan! :) (see #515209.) There was also an error in the second
    regex.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Change my email address.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Florian Schlichting ]
  * Added remove_scary_notice.patch.
  * Switched to source format 3.0 (quilt).
  * Refreshed debian/copyright.
  * Bumped Standards-Version to 3.9.3 (deleted versioning on ancient perl).
  * Bumped debhelper compatibility to 9 (use hardening flags).
  * Switched to short debian/rules.
  * Added build-dependency on libtest-pod-perl to enable t/99_pod.t.
  * Expanded long description by a few lines.
  * Added myself to Uploaders.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 04 Mar 2012 19:50:59 +0100

libterm-size-perl (0.2-4) unstable; urgency=low

  [ gregor herrmann ]
  The 'ready for perl 5.10' release.

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/control: add ${perl:Depends} (and ${misc:Depends}) to Depends to
    make the package pick up to correct dependency on perlapi; remove "-V"
    from dh_perl call in debian/rules.
  * Set Standards-Version to 3.7.3 (no changes).
  * Set debhelper compatibility level to 6.
  * debian/watch: improve regexp for matching upstream tarballs.
  * debian/copyright: rewrite from scratch.
  * debian/rules:
    - don't install empty /usr/share/perl5 directory if it exists
    - use $(CURDIR) instead of `pwd`
    - don't ignore errors of $(MAKE) realclean
    - move tests to build target
    - delete unused/unneeded dh_* calls
    - remove DESTDIR variable
    - move dh_clean before make distclean
    - use dh_clean to remove -stamp files
    - introduce install-stamp target depending on build-stamp
    - use "$@" for touching stamp files
    - use PREFIX and DESTDIR in call to make install
    - don't install README any more

  [ Damyan Ivanov ]
  * also remove unused dh_installdirs call

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 27 Jan 2008 20:49:02 +0100

libterm-size-perl (0.2-3) unstable; urgency=low

  * New Maintainer: Debian Perl Group.
  * Set Standards-Version to 3.7.2 (no changes).
  * Add watch file.
  * Activate tests in debian/rules.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 15 Sep 2006 00:59:25 +0200

libterm-size-perl (0.2-2) unstable; urgency=low

  * Adopting package (Closes: #357073).

 -- David Moreno Garza <damog@debian.org>  Thu, 16 Mar 2006 18:57:51 -0400

libterm-size-perl (0.2-1) unstable; urgency=low

  * Initial Release.

 -- Chip Salzenberg <chip@debian.org>  Sat, 15 Feb 2003 15:05:52 -0500
